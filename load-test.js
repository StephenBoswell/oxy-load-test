const http = require('https');
const faker = require('faker');

logResult = true;

const requestToMake = 100;
const rpm = 200;

const host = 'oxygen.dev.aws.ytech.co.nz';
const queryStart = '/dx/suggest/what/?query=';


const minute = 60 * 1000;

let requestsMade = 0;

const makeRequest = (res) => {
    http.request({
        protocol: 'https:',
        host,
        path: queryStart + faker.commerce.department()
    }, (res) => {
        if (logResult) {
            let str = '';
            res.on('data', (chunck => { str += chunck}));
            res.on('end', () => {console.log(str)});
        }

        if (requestsMade >= requestToMake) {
            process.exit();
        }
    }).end();
};

const scheduleRequest = function scheduleRequest() {
    if (requestsMade < requestToMake) {
        makeRequest();
        requestsMade++;
        console.log(`Making request ${requestsMade} of ${requestToMake}`);
        setTimeout(scheduleRequest, minute / rpm);
    }
}

scheduleRequest();